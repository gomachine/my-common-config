#!/bin/bash
# this should be part of the importing script (if desired) so the libs can be used for non critical things as well - since these settings are inherited..
#set -Eeuo pipefail

## some defaults
: ${MY_UPLINK:="uplink"}
: ${MY_NAMESERVERS_IPV4:="1.1.1.1 1.0.0.1"}
: ${MY_NAMESERVERS_IPV6:="2606:4700:4700::1111 2606:4700:4700::1001"}
#: ${MY_NAMESERVERS_DNS64:="2606:4700:4700::64 2606:4700:4700::6400"}		## cloudflare dns64 - cloudflare unfortunately is not as stable as google
: ${MY_NAMESERVERS_DNS64:="2001:4860:4860::6464 2001:4860:4860::64"}		## google dns64

: ${MY_NAT64_SUBNET:="64:ff9b::/96"}
: ${MY_NAT64_IPV6:="${MY_NAT64_SUBNET}"}
: ${MY_NAT64_IPV4:="192.168.255.0/24"}
: ${MY_VPN_IPV4:="192.168.254.0/24"}


function panic {
	echo "####" >&2
	echo "#### ERROR: $1" >&2
	echo "####" >&2
	exit 1
}

function debug {
	local _rc=$?
	[ -n "${MY_DEBUG:-}" ] && echo "## DEBUG: $1" >&2
	return ${_rc}
}

function get_var_string {
	## still having issues on this one. for some reason it overrides the original \$1 parameter and everything goes to shit. this is to blame on debconf I believe
	local _my_var=$1
	local _my_value
	local _my_debconf_path="shared/mygw"		## if you change this path it also needs to be updated in the debian template file
	local _my_debconf_var="${_my_var,,}"
	local _bashset=$-

	if [ -z "${!_my_var:-}" ] && [ -n "${ENVFILE:-}" ]; then		## only do this if ENVFILE var is actually set otherwise the grep and pipes below won't work
		set +u
		. /usr/share/debconf/confmodule

		db_register ${_my_debconf_path}/template_string ${_my_debconf_path}/${_my_debconf_var}
		db_subst ${_my_debconf_path}/${_my_debconf_var} myvar ${_my_var}
		db_fset ${_my_debconf_path}/${_my_debconf_var} seen false
		db_input high ${_my_debconf_path}/${_my_debconf_var}
		db_go
		db_get ${_my_debconf_path}/${_my_debconf_var}
		_my_value=$RET
		eval ${_my_var}='"${_my_value}"'		## be sure to use use single quotes as the variable needs to be quoted inside the eval as well to avoid problems on vars with spaces and other chars

		set -${_bashset}
		echo "## ${_my_var} set to ${_my_value}"
		grep -Eq "^ *${_my_var}=" ${ENVFILE}   ||   echo "${_my_var}=" >> ${ENVFILE}
		sed -i -e "s|^ *${_my_var}=.*|${_my_var}=${_my_value}|" ${ENVFILE}
	fi
	return 0
}

function get_ifname_from_mac {
	local _mac="${1}"
	ip -br link | grep ${_mac} | awk '{ print $1 }' | sed 's/@.*//' || panic "nic with mac ${_mac} not found on this system"
}

function get_mac_from_ifname {
	local _ifname="${1}"
	local _mac=$(ip -o link show dev ${_ifname} | awk '{ print $17 }') || debug "nic ${_ifname} not found" || return 1
	[ -z "${_mac}" ] && debug "unable to detect mac for nic ${_ifname}" && return 1
	echo "${_mac}"
}

function eui64 {
	local _mac="${1}"
	printf "%02x%s" $(( 16#${_mac:0:2} ^ 2#00000010 )) "${_mac:2}" \
		| sed -E -e 's/([0-9a-zA-Z]{2})*/0x\0|/g' \
		| tr -d ':\n' \
		| xargs -d '|' \
		printf "fe80::%02x%02x:%02xff:fe%02x:%02x%02x"
}

function gwmac {
	local _mac="${1}"
	printf "%02x%s" "0xfe" "${_mac:2}"
}

function testipv4 {
	local _ip4=${1:-}
	[[ ${_ip4} =~ ^[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}(/[0-9]{1,2}){0,1}$ ]] || debug "'${_ip4}' is not an IPv4" || return 1
}

function testipv6 {
	local _ip6=${1:-}
	[[ ${_ip6} =~ ^(([0-9a-fA-F]{1,4}:){7,7}[0-9a-fA-F]{1,4}|([0-9a-fA-F]{1,4}:){1,7}:|([0-9a-fA-F]{1,4}:){1,6}:[0-9a-fA-F]{1,4}|([0-9a-fA-F]{1,4}:){1,5}(:[0-9a-fA-F]{1,4}){1,2}|([0-9a-fA-F]{1,4}:){1,4}(:[0-9a-fA-F]{1,4}){1,3}|([0-9a-fA-F]{1,4}:){1,3}(:[0-9a-fA-F]{1,4}){1,4}|([0-9a-fA-F]{1,4}:){1,2}(:[0-9a-fA-F]{1,4}){1,5}|[0-9a-fA-F]{1,4}:((:[0-9a-fA-F]{1,4}){1,6})|:((:[0-9a-fA-F]{1,4}){1,7}|:)|fe80:(:[0-9a-fA-F]{0,4}){0,4}%[0-9a-zA-Z]{1,}|::(ffff(:0{1,4}){0,1}:){0,1}((25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])\.){3,3}(25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])|([0-9a-fA-F]{1,4}:){1,4}:((25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])\.){3,3}(25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9]))(/[0-9]{1,3}){0,1}$ ]] || debug "'${_ip6}' is not an IPv6" || return 1
}

function get_ip4_from_nic {
	local _nic=${1}
	local _type=${2:-static}
	local _ip
	case ${_type} in
		static)		_ip=$(ip -4 -o addr show dev ${_nic} scope global permanent -noprefixroute | awk '{ print $4 }' | head -n1) ;;
		dhcp)		_ip=$(ip -4 -o addr show dev ${_nic} scope global dynamic | awk '{ print $4 }' | head -n1) ;;
		any)		_ip=$(ip -4 -o addr show dev ${_nic} scope global | awk '{ print $4 }' | head -n1) ;;
	esac
	testipv4 "${_ip}" || debug "no ipv4 for ${_nic} found" || return 1
	echo "${_ip}"
}

function get_ip6_from_nic {
	local _nic=${1}
	local _type=${2:-static}
	local _class=${3:-global}
	local _iponly=${4:-}
	local _ip=""

	case ${_class} in
		global)	_class="^2" ;;
		ula)	_class="^fd" ;;
		*)		panic "unknown ip address class ${_class}"
	esac

	case ${_type} in
		static)		_ip=$(ip -6 -o addr show dev "${_nic}" scope global permanent -noprefixroute | awk '{ print $4 }' | head -n1) ;;						## static only
		dhcp)		_ip=$(ip -6 -o addr show dev "${_nic}" scope global | awk '{ print $4 }' | grep "${_class}" | grep    "\/128" | head -n1) ;;	## dhcp only
		slaacok)	_ip=$(ip -6 -o addr show dev "${_nic}" scope global | awk '{ print $4 }' | grep "${_class}" | grep -v "\/128" | head -n1) ;;	## static AND slaac/privacy ok
		link|ll|linklocal)
					_ip=$(ip -6 -o addr show dev "${_nic}" scope link | awk '{ print $4 }')  ;;
		*)			panic "unknown ip address type ${_type}"
	esac

	testipv6 "${_ip}" || debug "no ${_class}/${_type} ipv6 for ${_nic} found" || return 1
	[ "${_iponly}" == "iponly" ] && echo "${_ip%/*}" && return 0
	echo "${_ip}"
}

function get_subnet4_from_nic {
	local _nic=${1}
	local _ip4
	_ip4=$(get_ip4_from_nic "${_nic}") || debug "unable to get ipv4 subnet from ${_nic}" || return 1
	echo $(sipcalc "${_ip4}" | grep "Network address" | sed 's/.*\t- //')/$(sipcalc ${_ip4} | grep "Network mask (bits)" | sed 's/.*- //')
}

function get_subnet6_from_nic {
	local _nic=${1}
	local _ip6
	local _net
	_ip6=$(get_ip6_from_nic "${_nic}" slaacok) || debug "unable to get ipv6 subnet from ${_nic}" || return 1
	_net=$(sipcalc "${_ip6}" | grep "Subnet prefix (masked)" | sed 's/.*\t- //')
	testipv6 ${_net} || debug "unable to get ipv6 subnet from ${_nic}" || return 1
	echo ${_net}
}

function get_my_public_ip4 {
	local _ip4
	_ip4=$(dig -4 +short myip.opendns.com @resolver1.opendns.com)
	testipv4 ${_ip4} || debug "unable to my public IPv4 using IPv4 only" || return 1
	echo ${_ip4}
}

function get_my_primary_ip4 {
	local _ip4
	_ip4=$(ip -o route get 1 | awk '{ print $7 }' || /bin/true)
	testipv4 ${_ip4} || debug "unable to find my primary ipv4 address" || return 1
	echo ${_ip4}
}

function get_my_primary_ip6 {
	local _ip
	local _nic
	local _mynics
	_mynics=$(get_l3_nics)

	#_ip=$(ip -6 -o addr show scope global permanent -noprefixroute | awk '{ print $4 }' | head -n1)						## try first static/permenent IPs
	#testipv6 "${_ip}" || _ip=$(ip -6 -o addr show scope global permanent | awk '{ print $4 }' | grep "^2" | head -n1)		## if no static IPs is found, fall back to the first other *global* ip

	## first priority is a static IP
	## second dhcp assigned
	## third slaac
	for _nic in ${_mynics}; do
		get_ip6_from_nic "${_nic}" static global iponly && debug "found static IPv6" && return 0
	done
	for _nic in ${_mynics}; do
		get_ip6_from_nic "${_nic}" dhcp global iponly && debug "found dhcp IPv6" && return 0
	done
	for _nic in ${_mynics}; do
		get_ip6_from_nic "${_nic}" slaacok global iponly && debug "found slaac IPv6" && return 0
	done
	debug "no IPv6 found"
	return 1
}

function get_l3_nics {
	local _nics
	#_nics=$(ip -6 -o addr show scope global permanent -noprefixroute | awk '{ print $2 }')		## only nics with static/permenent IPs
	_nics=$(ip -6 -o addr show scope global | awk '{ print $2 }' | uniq)				## incl all nics, inc those with only slaac/dhcp ips
	[ -z "${_nics}" ] && debug "no l3 nics found" && return 1
	echo ${_nics}
}

function get_hostname {
	local _hostname
	_hostname=$(hostname)
	[ "${_hostname}" == "${_hostname/.}" ] && debug "not a fqdn hostname" && return 1
	echo ${_hostname}
}

function get_domainname {
	local _domainname
	[ -z "${_domainname:-}" ] && _domainname=$(cat /etc/resolv.conf | sed -e '/^domain /!d;s/^domain //')
	[ -z "${_domainname:-}" ] && _domainname=$(hostname -d)
	[ -z "${_domainname:-}" ] && debug "unable to get domain name" && return 1
	echo ${_domainname}
}

function get_subnet6_n {
	local _subnet=$1
	local _size=$2
	local _n=$3
	## this could be limited gracefully in sed for that matter but it could output incorrect values in that case. so not ideal/ bug windows.
	## with IPv6 we should just start off of a smaller block if we need to subnet. it makes no sense to cut off such a big pie into such small pieces.
	## so this should really break regardless of CPU/time problems
	[ "${_subnet#*/}" -lt "$((${_size}-16))" ] && panic "to big of a scope for a too small subnet. /${_size} out of /${_subnet#*/} trust me you don't want me to run this through sipcalc. but happy to take suggestions for a better way....."
	local _raw=$(sipcalc --v6split=/${_size} ${_subnet} | grep "^Network" 2>/dev/null | sed -e 's/.* \([0-9a-f:]\+\) .*/\1/' -e "${_n} !d")
	_raw=$(sipcalc ${_raw}/${_size} | grep "Compressed address" | sed 's/.*\t- //')
	[ -z "${_raw}" ] && debug "unable to get the ${_n} subnet of size ${_size} in subnet ${_subnet}" && return 1
	echo ${_raw}
}

function get_subnet4_n {
	local _subnet=$1
	local _size=$2
	local _n=$3
	local _raw=$(sipcalc --v4split=/${_size} ${_subnet} | grep "^Network" 2>/dev/null | sed -e 's/.* \(\([0-9]\{1,3\}\.*\)\{4\}\) .*/\1/' -e "${_n} !d")
	[ -z "${_raw}" ] && debug "unable to get the ${_n} subnet of size ${_size} in subnet ${_subnet}" && return 1
	echo ${_raw}
}

function gen_resolv_conf {
	echo "domain ${MY_DOMAINNAME%.}"
	echo "search ${MY_DOMAINNAME%.}."
	if systemctl is-enabled bind9 >/dev/null 2>&1; then
		echo "nameserver ::1"
	elif [ -n "$(ip -6 route list match :: 2>/dev/null)" ]; then
		for dns in ${MY_NAMESERVERS_IPV6}; do
			echo "nameserver ${dns}"
		done
	else
		for dns in ${MY_NAMESERVERS_IPV4}; do
			echo "nameserver ${dns}"
		done
	fi
}

function set_hostname {
	local _hostname=${MY_HOSTNAME}
	local _domainname=${MY_DOMAINNAME%.}

	## just a testing hook: simply return doing nothing if testing var is set to non-empty
	[ -n "${MY_HOSTNAME_TEST:-}" ] && echo ${_hostname}.${_domainname} && return 0

	[ -n "${_domainname}" ] && _hostname=${_hostname}.${_domainname}
	hostnamectl set-hostname ${_hostname} && return 0
	echo ${_hostname} >/etc/hostname
	hostname -F /etc/hostname
}

## generate ifupdown interfaces section for this interface
function gen_interface_config {
	local _mgmt_vrf_name=mgmtvrf
	local _mgmt_vrf_rtable=mgmt

	local _nic=${1}
	local _mode=MY_${_nic^^}_MODE
	local _ip4=MY_${_nic^^}_IPV4
	local _ip6=MY_${_nic^^}_IPV6
	local _gw4=MY_${_nic^^}_GWV4
	local _gw6=MY_${_nic^^}_GWV6
	local _mac=MY_${_nic^^}_MAC
	local _vlan=MY_${_nic^^}_VLAN
	local _ssid=MY_${_nic^^}_WIFI
	local _psk=MY_${_nic^^}_WIFI_PSK
	local _slaves=MY_${_nic^^}_SLAVES
	local _wifi_model=MY_${_nic^^}_WIFI_MODEL
	local _ip4_server=MY_${_nic^^}_IPV4_SERVER
	local _ap_config=MY_${_nic^^}_AP_CONFIG
	local _gwmac
	local _eui64
	local _trunk
	local _vlan_tag

	echo "## ${_nic} block ##"
	echo "auto ${_nic}"

	if [ "${_nic}" == "lo" ]; then
		echo "iface lo inet loopback"
	else
		echo "allow-hotplug ${_nic}"
	fi

	## for ifupdown all additional "nic-wide" configs need to be part of whatever the first address family is
	## so I'm gonna go ahead and force IPv6 config, while IPv4 is optional
	## which is why IPv4 config is below all other addon configs, and IPv6 on top
	_ip6=${!_ip6:-}
	case "${_ip6}" in
		dhcp)
			echo "iface ${_nic} inet6 dhcp"
			;;

		manual)
			echo "iface ${_nic} inet6 manual"
			;;

		unnumbered)
			_mac6=${!_mac:-}
			[ -z "${_mac6}" ] && _mac6=$(get_mac_from_ifname ${_nic})
			_gwmac=$(gwmac ${_mac6})		## need to do this here otherwise exit code does not catch
			_eui64=$(eui64 ${_gwmac})
			cat <<-EOF
				iface ${_nic} inet6 manual
				    up ip -6 route add default dev \${IFACE} via ${_eui64}
			EOF
			;;

		slaac)
			echo "iface ${_nic} inet6 auto"
			;;
		*)
			if testipv6 "${_ip6}"; then
				if [ -n "${!_ip4_server:-}" ]; then
					echo "iface ${_nic} inet6 v4tunnel"
				else
					echo "iface ${_nic} inet6 static"
					echo "    dad-attempts 0"
				fi
				echo "    address ${_ip6}"
				[ -n "${!_gw6:-}" ] && echo "    gateway ${!_gw6}"
				[ -n "${!_ip4_server:-}" ] && echo "    endpoint ${!_ip4_server}"
			fi
			;;
	esac

	## weird shit we have to do on the r2, maybe/hopefully we only need it for this one
	if [ "${!_wifi_model:-}" == "bpi-r2" ]; then
		echo "    pre-up /usr/sbin/init-r2-wifi"
	fi

	## special interface modes
	_mode=${!_mode:-}			## recursive lookup and set to empty if not set
	_mode=${_mode,,}			## make it all lowercase
	case "${_mode}" in
		mgmt)
			debug "vrf enabled. services need to be bound to the vrf specifically to repond. or net.ipv4.tcp_l3mdev_accept=1 to ignore vrf all together on wildcard socket binds. but that's not idea for security" || /bin/true
			cat <<-EOF
			    pre-up /bin/ip link add ${_mgmt_vrf_name} type vrf table ${_mgmt_vrf_rtable}
			    pre-up /bin/ip link set up dev ${_mgmt_vrf_name}
			    pre-up /bin/ip link set master ${_mgmt_vrf_name} dev ${_nic}
			    up service sshd@${_mgmt_vrf_name} start
			    down service sshd@${_mgmt_vrf_name} stop
			    post-down /bin/ip link del dev ${_mgmt_vrf_name}
			EOF
	;;
	esac

	## is the nic supposed to be a wifi client?
	if [ -n "${!_ssid:-}" ]; then
		_ssid=${!_ssid}
		_psk=${!_psk}
		cat <<-EOF
		    wpa-ssid "${_ssid}"
		    wpa-psk "${_psk}"
		EOF
	fi

	## is the nic supposed to be a wifi AP?
	[ -n "${!_ap_config:-}" ] && echo "    hostapd ${!_ap_config}"

	## is this a virtual bridge interface? if so add neccessary stuff
	if [ -n "${!_slaves:-}" ]; then
		_slaves=${!_slaves}
		cat <<-EOF
		    bridge_ports ${_slaves}
		    bridge_fd 5
		    bridge_stp no
		EOF
	fi

	## is the nic supposed to be a vlan
	if [ -n "${!_vlan:-}" ]; then
		_vlan=${!_vlan}
		_trunk=${_vlan%.*}
		_vlan_tag=${_vlan#*.}
		echo "    pre-up /bin/ip link add link ${_trunk} name \${IFACE} type vlan id ${_vlan_tag} loose_binding on"
		echo "    post-down /bin/ip link del dev \${IFACE}"
	fi

	## for ifupdown all additional "nic-wide" configs need to be part of whatever the first address family is
	## so I'm gonna go ahead and force IPv6 config, while IPv4 is optional
	## which is why IPv4 config is below all other addon configs, and IPv6 on top
	_ip4=${!_ip4:-}		## recursive lookup and make it all lowercase
	case "${_ip4}" in
		dhcp)
			echo "iface ${_nic} inet dhcp"
			;;

		manual)
			echo "iface ${_nic} inet manual"
			;;

		unnumbered)
			_mac4=${!_mac:-}
			[ -z "${_mac4}" ] && _mac4=$(get_mac_from_ifname ${_nic})
			_gwmac=$(gwmac ${_mac4})		## need to do this here otherwise exit code does not catch
			cat <<-EOF
				iface ${_nic} inet manual
				    up sysctl -w net.ipv4.conf.\${IFACE}.arp_ignore=8
				    up ip -4 neigh add 169.254.0.1 lladdr ${_gwmac} nud permanent dev \${IFACE}
				    up ip -4 route add default dev \${IFACE} via 169.254.0.1 onlink
			EOF
			;;

		*)
			if testipv4 "${_ip4}"; then
				echo "iface ${_nic} inet static"
				echo "    address ${_ip4}"
				[ -n "${!_gw4:-}" ] && echo "    gateway ${!_gw4}"
			fi
			;;
	esac

	echo
	echo
}

function gen_net_udev_rule {
	local _nic=${1}
	local _mac=MY_${_nic^^}_MAC
	local _os_name

	[ -z "${!_mac:-}" ] && debug "no mac set for ${_nic}, assuming its virtual or we re just using the system default name" && return 0

	_mac=${!_mac}
	cat <<-EOF
		SUBSYSTEM=="net", ACTION=="add", DRIVERS=="?*", ATTR{address}=="${_mac}", ATTR{type}=="1", NAME="${_nic}"
	EOF
}

function get_subnet6_range {
	## probably only needed for dhcpd6 pd config
	local _subnet=$1
	local _size=$2
	
	[ "${_subnet#*/}" -lt "$((${_size}-16))" ] && panic "to big of a scope for a too small subnet. /${_size} out of /${_subnet#*/} trust me you don't want me to run this through sipcalc. but happy to take suggestions for a better way....."
	local _raw=$(sipcalc --v6split=/${_size} ${_subnet} | grep "^Network" 2>/dev/null | sed -e 's/.* \([0-9a-f:]\+\) .*/\1\/96/' -e 1b -e '$!d' | xargs sipcalc | grep "Compressed address" | sed 's/.*\t- //')
	[ -z "${_raw}" ] && debug "unable to get range of size ${_size} in subnet ${_subnet}" && return 1
	echo ${_raw}
}

function get_ipv4_nameserver_for_nic {
	local _nic=$1
	local _nameserver=MY_${_nic^^}_NAMESERVER_IPV6

	## specifically configured Nameserver of this interface has priority
	_nameserver=${!_nameserver:-}

	## if I have my own nameserver, just use that, regardless of ipv4/6 situation
	[ -z "${_nameserver}" ] && systemctl is-enabled bind9 >/dev/null 2>&1 && _nameserver=$(get_my_primary_ip4)

	## if I have ipv6 on this nic screw DNS4 all together. this helps for nameserver that are reachable on IPv6 only. as they would get wronly negative cache if querried over ipv4
	[ -z "${_nameserver}" ] && [ -z "$(ip -o -6 addr show scope global dev ${_nic})" ] && return 1

	## looks like I have no dedicated dns, nor run my own, nor IPv6 at all. so falling back to my default IPv4
	[ -z "${_nameserver}" ] && _nameserver=${MY_NAMESERVERS_IPV4}

	echo "${_nameserver}"
}

function get_ipv6_nameserver_for_nic {
	local _nic=$1
	local _nameserver=MY_${_nic^^}_NAMESERVER_IPV6

	_nameserver=${!_nameserver:-}			## specifically configured Nameserver of this interface has priority

	[ -z "${_nameserver}" ] && systemctl is-enabled bind9 >/dev/null 2>&1 && _nameserver=$(get_my_primary_ip6)			## if I have my own nameserver, just use that, regardless of ipv4/6 situation
	[ -z "${_nameserver}" ] && [ -z "$(ip -o -4 addr show dev ${_nic})" ] && _nameserver=${MY_NAMESERVERS_DNS64}		## if I have ipv6 only use dns64
	[ -z "${_nameserver}" ] && _nameserver=${MY_NAMESERVERS_IPV6}														## looks like I have no dedicated dns, nor run my own, and I have dual stack. so gonna use regular ipv6 nameservers

	echo "${_nameserver}"
}

function gen_dhcpd6_subnet_from_nic {
	local _nic=$1
	local _range_pd=MY_${_nic^^}_IPV6_DHCPPD
	local _domainname=MY_${_nic^^}_DOMAINNAME
	local _range_bitlen=96
	local _subnet6
	local _range
	local _range_temp

	_subnet6="$(get_subnet6_from_nic ${_nic})" || return 1
	_range=$(get_subnet6_n ${_subnet6/\/64/\/80} ${_range_bitlen} 1) || return 1		## first /96 for stateful leases
	_range_temp=$(get_subnet6_n ${_subnet6/\/64/\/80} ${_range_bitlen} 2) || return 1	## second /96 for privacy/temp leases

	if [ -n "${!_range_pd:-}" ]; then
		_range_pd="$(get_subnet6_range ${!_range_pd} ${_range_bitlen}) /${_range_bitlen}"
	else
		_range_pd=
	fi

	_nameserver=$(get_ipv6_nameserver_for_nic ${_nic})
	_nameserver=${_nameserver// /,}
	_domainname=${!_domainname:-${MY_DOMAINNAME}}

	echo "subnet6 ${_subnet6} {"
	echo "    range6 ${_range}/${_range_bitlen};"
	echo "    range6 ${_range_temp}/${_range_bitlen} temporary;"
	[ -n "${_range_pd:-}" ] && echo "    prefix6 ${_range_pd};"
	echo "    option dhcp6.domain-search \"${_domainname}\";"
	echo "    option dhcp6.name-servers ${_nameserver};"
	echo "}"
}

function gen_dhcpd4_subnet_from_nic {
	local _nic=$1
	local _start=10
	local _end=100
	local _gw4
	local _subnet4
	local _mask4
	local _nodns4
	local _nameserver=MY_${_nic^^}_NAMESERVER_IPV4
	local _domainname=MY_${_nic^^}_DOMAINNAME

	_gw4=$(get_ip4_from_nic "${_nic}") || return 1
	_subnet4=$(get_subnet4_from_nic "${_nic}") || return 1
	_mask4=$(sipcalc "${_subnet4}" | grep -P "Network mask\t" | sed 's/.*\t- //') || return 1
	_domainname=${!_domainname:-${MY_DOMAINNAME}}

	_nameserver=$(get_ipv4_nameserver_for_nic ${_nic}) || _nodns4=true
	_nameserver=${_nameserver// /,}

	echo "subnet ${_subnet4%/*} netmask ${_mask4} {"
	echo "    range ${_subnet4%.*}.${_start} ${_subnet4%.*}.${_end};"
	echo "    option routers ${_gw4%/*};"
	echo "    option domain-name \"${_domainname}\";"
	[ -z "${_nodns4:-}" ] && echo "    option domain-name-servers ${_nameserver};"
	echo "}"
}

function get_ipv6_only_nics {
	diff --new-line-format="" --unchanged-line-format="" <(ip -o -6 addr show scope global | awk '{ print $2 }' | sort) <(ip -o -4 addr show scope global | awk '{ print $2 }' | sort)
}
