#!/bin/bash
set -Eeuo pipefail

: ${REPONAME:="$1"}
: ${PROVIDER:="gitlab.com"}
: ${PROJECT:="ybot"}
: ${DEPS:="my-common-config"}
: ${MAINTAINER:="toby <toby@gmx.it>"}


mkdir $REPONAME
mkdir $REPONAME/files
mkdir $REPONAME/debian
mkdir $REPONAME/debian/source

curl -o $REPONAME/LICENSE https://www.gnu.org/licenses/gpl-3.0.txt
ln -s ../LICENSE $REPONAME/debian/copyright

echo "11" >$REPONAME/debian/compat
echo "3.0 (native)" >$REPONAME/debian/source/format

cat <<-EOF >$REPONAME/debian/control
	Source: $REPONAME
	Section: config
	Priority: extra
	Maintainer: ${MAINTAINER}
	Build-Depends: debhelper (>= 7.0.0~), config-package-dev (>= 4.15)
	Standards-Version: 3.9.2
	Vcs-Browser: https://${PROVIDER}/$PROJECT/$REPONAME
	Vcs-Git: https://${PROVIDER}/$PROJECT/$REPONAME
	
	Package: $REPONAME
	Architecture: all
	Depends: $DEPS
	Provides: \${diverted-files}
	Conflicts: \${diverted-files}
	Description: please update me
EOF


cat <<"EOF" >$REPONAME/debian/rules
#!/usr/bin/make -f

%:
	dh $@ --with=config-package


# Prevent dh_installdeb of treating files in /etc as configuration files
# you need this if need configuration files been always rewritten
# even if changed
override_dh_installdeb:
	dh_installdeb
#	rm -f debian/*/DEBIAN/conffiles
EOF

chmod 755 $REPONAME/debian/rules


cat <<-"EOF" >$REPONAME/.gitignore
	debian/.debhelper
	debian/debhelper-build-stamp
	debian/*/
	!debian/source/
	debian/files
	debian/*.postinst.debhelper
	debian/*.prerm.debhelper
	debian/*.substvars
	debian/*.debhelper.log
	debian/*.postrm.debhelper
	debian/changelog
EOF


cd $REPONAME
git init
git add .gitignore debian/ LICENSE
git remote add origin git@${PROVIDER}:${PROJECT}/${REPONAME}.git
