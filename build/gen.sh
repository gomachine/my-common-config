#!/bin/bash
set -e
DEBNAME=${PWD##*/}

upload="repo.mitico.us:/var/www/html/repo-incoming"
my_name="$(git config --get user.name)"
my_email="$(git config --get user.email)"

echo -en "$DEBNAME (2.0.$(TZ=UTC date +%Y.%m.%d.%H.%M)) unstable; urgency=low\n\n$(git log --format='  * %s')\n\n -- ${my_name} <${my_email}>  $(TZ=UTC date -R)" >debian/changelog
dpkg-buildpackage --sign-key=${my_email}
my_files="$(find ../ -maxdepth 1 -type f -name ${DEBNAME}_*)"
rsync -Pi ${my_files} ${upload}
rm ${my_files}

ssh bastion.mitico "sudo /etc/reprepro/publish.sh"
